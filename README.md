# Software requirements #

Ubuntu 20.04

ROS noetic

Gazebo 11

git

# Setup #

## GitLab ##
We will rely on gitlab and it is mandatory to use it for this coursework.
GitLab is an online repository that can be used with the git control revision system.
The CS department runs a GitLab hosting service, and all students should be able to access it with their CS account.
If you don't have a CS account, [register here](https://www.cs.mcgill.ca/docs/)

Important: do not share your code and repository with anyone and keep your source code secret.
If we identify that two students have identical portion of code, both will be considered to have cheated.


## Create your own repository
We are going to be using the Git revision control system during the course.
If you use your own machine then make sure to install Git.

You will need to create a repository and use `applied-robotics-f2024` as your repository name.  
Please make sure your repository is *private*.

Now, grant access to the teaching staff

![Granting the teaching staff read access](/figures/gl_permissions1.png "Granting the teaching staff read access.")

You should grant the following users *Reporter* access:
  * Hsiu-Chin LIN (gitlab ID: linhz)
  * Mohamad Danesh (gitlab ID: mohamad.danesh)
  

  
Next, you will have to clone the repository to your local machine. You can clone the repository using either HTTPS or SSH.
Using SSH is more secure, but requires
[uploading a private key to GitLab](https://docs.gitlab.com/ee/user/ssh.html). 

HTTPS is less secure but simpler as it only
requires you to enter your CS account username and password. If in doubt, SSH is easier.

In order to clone the repository via SSH you should ensure that you've uploaded a private key to GitLab, launch a terminal, and type:

```
$ git clone git@gitlab.cs.mcgill.ca:(YOUR-CS-ID)/applied-robotics-f2024.git
```

where (YOUR-CS-ID) is your CS gitlab account id. (Note that, this may not be the same with your username)


In order to clone the repository via HTTPS you should launch a terminal and type:

```
$ git clone https://gitlab.cs.mcgill.ca/(YOUR-CS-ID)/applied-robotics-f2024.git
```

where (YOUR-CS-ID) is your CS  gitlab account id as above, and you should be prompted to type in your CS gitlab account id and password.


## Working with git and pushing your changes

We suggest you follow the excelent [tutorial](https://www.atlassian.com/git/tutorials/what-is-version-control) from atlassian on how to use git. In particular you will need to understand the following basic meachnisms:

* [add and commit](https://www.atlassian.com/git/tutorials/saving-changes)
* [push](https://www.atlassian.com/git/tutorials/syncing/git-push)



## Register your student id and name

We will need you fill up [this google form](https://forms.office.com/Pages/ResponsePage.aspx?id=cZYxzedSaEqvqfz4-J8J6kknQG5pFmJBqod1wxgyl31UNU5ROFZES05WTzczSDUyUTZFRkRHTDMyMS4u)
in order for us to mark your assignments. 
You may be asked to login with your McGill credential.
